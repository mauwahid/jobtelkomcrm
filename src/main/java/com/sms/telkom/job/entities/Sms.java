package com.sms.telkom.job.entities;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.util.Date;


@MappedSuperclass
@Data
public class Sms extends AbstractEntity{


    private Long id;

    private String content;

    private String contentToCustomer;

    private String senderId;

    private String senderIdToCust;

    private String msisdn;

    private String operator;

    private int type;

    private int sentStatus;

    private String deliveryInfo;

    private Date deliveredDate;

    private long idUser;

    private String gateway;

    private String vendorTrxId;

    private int smsCount;

    private String smsErrorResponse;



}
