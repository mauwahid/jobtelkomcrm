package com.sms.telkom.job.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
public class SmsResolvedActive extends Sms {

    @Column(unique = true)
    private String ticketNum;


    //answered
    private Date answeredDate;

    private byte isAnswered;

    private String answer;

    private String replyContent;

    private String shortCode;

    private int isSentToTelkom;

    private Date sentToTelkomDate;

    private int sentToTelkomStatus;

    private String sentToTelkomDesc;
}
