package com.sms.telkom.job.repositories;

import com.sms.telkom.job.entities.SmsResolvedActive;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmsResolvedActiveRepository extends PagingAndSortingRepository<SmsResolvedActive,Long> {


    @Query("select sms from SmsResolvedActive sms  where (sms.sentToTelkomStatus = 0 or sms.sentToTelkomStatus = 2) and sms.isSentToTelkom < 4")
    List<SmsResolvedActive> findBySentToTelkom();
}
