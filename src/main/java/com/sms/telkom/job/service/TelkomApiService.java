package com.sms.telkom.job.service;

import com.sms.telkom.job.dtos.SmsApiResponse;
import com.sms.telkom.job.utils.Common;
import lombok.extern.slf4j.Slf4j;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class TelkomApiService {


    private String url = "http://servicebus.telkom.co.id:9001/CEMProject/TelkomComposite/Assurance/ProxyService/smsFaultHandling";

    public SmsApiResponse sendSms(String trxId, String msisdn, String ticketNum, String answer) {

        SmsApiResponse smsApiResponse = new SmsApiResponse();

        Map<String, String> params = new HashMap<>();
        params.put("msisdn", msisdn);
        params.put("trx_id", trxId);
        params.put("sms", "TLKM "+answer);
        params.put("ticketid", ticketNum);

        String response = "";
        try{
            response = sendRequest(url, params);

            log.info("response "+response);

            if(response.toLowerCase().contains("<status>1</status>")){
                smsApiResponse.setStatus(Common.STATUS_SUCCESS);
            }else {
                smsApiResponse.setStatus(Common.STATUS_FAILED);
            }
            smsApiResponse.setStatusDesc(response);



        }catch (Exception ex){
            response = ex.toString();
            smsApiResponse.setStatus(Common.STATUS_FAILED);
            smsApiResponse.setStatusDesc(response);

        }
        return smsApiResponse;
    }


    private  String sendRequest(String uri, Map<String, String> params)
            throws IOException {

        //  OkHttpClient client = new OkHttpClient();
        log.debug("URI "+uri.toString());
        // uri = uri+"lion/search/best_price";
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        OkHttpClient client = builder.build();

        HttpUrl.Builder urlBuilder = HttpUrl.parse(uri).newBuilder();

        params.forEach((k,v)->
                {
                    urlBuilder.addQueryParameter(k,v);

                }
        );

        String url = urlBuilder.build().toString();

        log.debug("url : "+url);

        Request request = new Request.Builder().
                url(url).build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }





}
