package com.sms.telkom.job.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@Service
@Slf4j
public class ArchivingService {

    @PersistenceContext
    private EntityManager entityManager;


    public void executeMoveArchive(){
        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("move_to_archive");
        log.debug("start executing move_to_archvie");
        query.execute();
        log.debug("end executing move_to_archvie");

    }
}
