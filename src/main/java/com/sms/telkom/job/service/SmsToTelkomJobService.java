package com.sms.telkom.job.service;

import com.sms.telkom.job.dtos.SmsApiResponse;
import com.sms.telkom.job.entities.SmsResolvedActive;
import com.sms.telkom.job.repositories.SmsResolvedActiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class SmsToTelkomJobService {


    @Autowired
    private SmsResolvedActiveRepository smsResolvedActiveRepository;

    @Autowired
    private TelkomApiService telkomApiService;



    public void runningService(){

        List<SmsResolvedActive> list = smsResolvedActiveRepository.findBySentToTelkom();

        list.forEach(sms->{
            sendSmsInfo(sms);
        });


    }


    private void sendSmsInfo(SmsResolvedActive smsResolvedActive){

        SmsApiResponse apiResponse = telkomApiService.sendSms(smsResolvedActive.getVendorTrxId(), smsResolvedActive.getMsisdn(),
                smsResolvedActive.getTicketNum(), smsResolvedActive.getAnswer());

        int sentToTelkomCount = smsResolvedActive.getIsSentToTelkom();

        smsResolvedActive.setSentToTelkomStatus(apiResponse.getStatus());
        smsResolvedActive.setSentToTelkomDate(new Date());
        smsResolvedActive.setIsSentToTelkom(sentToTelkomCount+1);
        smsResolvedActive.setSentToTelkomDesc(apiResponse.getStatusDesc());

        smsResolvedActiveRepository.save(smsResolvedActive);

    }










}
