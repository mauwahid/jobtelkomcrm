package com.sms.telkom.job.dtos;

import lombok.Data;

@Data
public class SmsApiResponse implements ResponseInterface {


    private int status;

    private String trxId;

    private String statusDesc;

    private String gatewayName;
}
