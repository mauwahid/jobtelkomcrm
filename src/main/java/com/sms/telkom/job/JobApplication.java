package com.sms.telkom.job;

import com.sms.telkom.job.service.ArchivingService;
import com.sms.telkom.job.service.SmsToTelkomJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class JobApplication {

	@Autowired
	private SmsToTelkomJobService smsToTelkomJobService;

	@Autowired
	private ArchivingService archivingService;

	public static void main(String[] args) {
		SpringApplication.run(JobApplication.class, args);
	}


	@Scheduled(fixedDelay = 1800000)
	public void startTelkomJob(){
		smsToTelkomJobService.runningService();
	}

	@Scheduled(fixedDelay = 3600000)
	public void startArchiving(){
		archivingService.executeMoveArchive();
	}


}
